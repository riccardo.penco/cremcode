QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += $$PWD/src

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    src/language/canvas.cpp \
    src/language/floodfiller.cpp \
    src/language/instruction.cpp \
    src/language/language.cpp \
    src/language/parser.cpp \
    src/language/program.cpp \
    src/language/utils.cpp \
    src/language/writer.cpp \
    src/utils/recentfiles.cpp \
    src/widgets/cremcanvas.cpp \
    src/widgets/cremconsole.cpp \
    src/widgets/infodlg.cpp

HEADERS += \
    mainwindow.h \
    src/language/canvas.h \
    src/language/floodfiller.h \
    src/language/instruction.h \
    src/language/language.h \
    src/language/parser.h \
    src/language/program.h \
    src/language/utils.h \
    src/language/writer.h \
    src/utils/recentfiles.h \
    src/version.h \
    src/widgets/constants.h \
    src/widgets/cremcanvas.h \
    src/widgets/cremconsole.h \
    src/widgets/infodlg.h

FORMS += \
    mainwindow.ui \
    src/widgets/cremcanvass.ui \
    src/widgets/cremconsole.ui \
    src/widgets/infodlg.ui

TRANSLATIONS += \
    CremCode_en_150.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    src/resources.qrc

RC_FILE = src/cremcode.rc

DISTFILES += \
    src/cremcode.rc
