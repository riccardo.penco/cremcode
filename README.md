## Crem Code
## Simple program to paint simple drawings using a simple programming language
This program is intended for elementary school children with the supervision of a teacher or an adult.  

The commands to control the painting are:
* Pen UP - lifts the pen
* Pen DOWN - lowers the pen
* Move LEFT N - moves the cursor left by N units, painting in the current color if the pen is down
* Move RIGHT N - moves the cursor right by N units, painting in the current color if the pen is down
* Move UP N - moves the cursor up by N units, painting in the current color if the pen is down
* Move DOWN N - moves the cursor down by N units, painting in the current color if the pen is down
* Move TO X Y - moves the cursor to the position specified by X and Y coordinates, painting in the current color if the pen is down
* Set COLOR C - sets C as the painting color
* Fill - fills the region around the cursor with the current color

The colors available are:
* Black
* White
* Dark Gray
* Gray
* Light Gray
* Red
* Green
* Blue
* Cyan
* Magenta
* Yellow
* Dark Red
* Dark Green
* Dark Blue
* Dark Cyan
* Dark Magenta
* Dark Yellow

In the *examples* folder there are some simple programs.  
Crem Code at the moment lacks an editor to write the programs, so programs must be written with an external text editor (extension .crem or .txt) and loaded.
