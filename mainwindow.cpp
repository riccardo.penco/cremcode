#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "language/language.h"
#include "language/parser.h"
#include "widgets/infodlg.h"

#include <QSettings>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(qAppName());
    prepareRecentFilesActions();
    readRecentFilesFromSettings();
    updateRecentFilesActions();
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::maybeOpenProgram);
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::about);
    connect(ui->actionQuit, &QAction::triggered, this, &MainWindow::close);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::prepareRecentFilesActions()
{
    for (auto n = 0; n < recentFilesCount; ++n)
    {
        auto *act = new QAction(this);
        act->setShortcut(QStringLiteral("Ctrl+%1").arg(n + 1));
        act->setVisible(false);
        m_recentFilesActions.push_back(act);
        ui->menuRecentFiles->addAction(act);
        connect(act, &QAction::triggered, this, [this, n](){ openRecentFile(n); });
    }
}

void MainWindow::updateRecentFilesActions()
{
    const auto &files =  m_recentFiles.list();
    const auto numFiles = static_cast<unsigned short>(files.size());

    for (auto n = 0u; n < numFiles; ++n)
    {
        auto &act = m_recentFilesActions.at(n);
        act->setText(QString("%1 - %2").arg(n + 1).arg(files.at(n).fileName()));
        act->setVisible(true);
    }
    for (auto n = numFiles; n < recentFilesCount; ++n)
        m_recentFilesActions.at(n)->setVisible(false);
}

void MainWindow::readRecentFilesFromSettings()
{
    auto settings = QSettings(settingsFileName, QSettings::IniFormat);
    auto files = settings.value(recentFilesSettingsKey).toStringList();
    while (files.size() > recentFilesCount)
        files.removeLast();
    for (auto it = files.crbegin(); it != files.crend(); ++it)
        m_recentFiles.addFile(*it);
}

void MainWindow::saveRecentFilesToSettings()
{
    auto files = QStringList();
    for (const auto &file : m_recentFiles.list())
        files.append(file.fileFullPath());
    auto settings = QSettings(settingsFileName, QSettings::IniFormat);
    settings.setValue(recentFilesSettingsKey, files);
}

void MainWindow::maybeOpenProgram()
{
    const auto fileName = QFileDialog::getOpenFileName(this,
                                                       tr("Open Program"),
                                                       {},
                                                       tr("CremCode Program (*.crem);;Text File(*.txt)"));
    if (fileName.isEmpty())
        return;

    openProgram(fileName);
}

void MainWindow::errorBox(const QString &msg, const QString &details)
{
    QMessageBox box;
    box.setIcon(QMessageBox::Critical);
    box.setText(msg);
    if (!details.isEmpty())
        box.setDetailedText(details);
    box.setStandardButtons(QMessageBox::Ok);
    box.setDefaultButton(QMessageBox::Ok);
    box.exec();
}

void MainWindow::warningBox(const QString &msg, const QString &details)
{
    QMessageBox box;
    box.setIcon(QMessageBox::Warning);
    box.setText(msg);
    if (!details.isEmpty())
        box.setDetailedText(details);
    box.setStandardButtons(QMessageBox::Ok);
    box.setDefaultButton(QMessageBox::Ok);
    box.exec();
}

void MainWindow::about()
{
    auto info = InfoDlg(ui->cremConsole->maxCanvasSize());
    info.exec();
}

void MainWindow::openRecentFile(int idx)
{
    openProgram(m_recentFiles.list().at(idx).fileFullPath());
}

void MainWindow::openProgram(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::OpenModeFlag::ReadOnly))
        errorBox(tr("Error while opening the file '%1'").arg(QDir::toNativeSeparators(fileName)), file.errorString());

    const auto code = QString::fromUtf8(file.readAll());
    auto parser = crem::Parser();
    const auto res = parser.parseProgram(code);
    if (res.program.size() == 0)
    {
        errorBox(tr("Error while parsing the file"), tr("List of all errors:\n%1").arg(res.errors.join("\n")));
        return;
    }
    ui->cremConsole->setProgram(res.program);
    if (!res.errors.isEmpty())
        warningBox(tr("The program was loaded but there were some problems"), tr("List of all problems:\n%1").arg(res.errors.join("\n")));

    m_recentFiles.addFile(fileName);
    updateRecentFilesActions();
    saveRecentFilesToSettings();
}

