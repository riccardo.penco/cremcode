#include "mainwindow.h"

#include <QApplication>
#include <QStyleFactory>

#include "version.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setStyle(QStringLiteral("Fusion")); // {"windowsvista | "macintosh"}, "Windows", "Fusion"
    a.setApplicationName(VER_INTERNALNAME_STR);
    a.setApplicationVersion(VER_PRODUCTVERSION_STR);
    a.setOrganizationName(VER_COMPANYNAME_STR);
    a.setOrganizationDomain(VER_COMPANYDOMAIN_STR);
    a.setWindowIcon(QIcon(QStringLiteral(":/img/cremcode.png")));

    MainWindow w;
    w.showMaximized();

    return a.exec();
}
