#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "utils/recentfiles.h"

#include <QAction>
#include <vector>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void prepareRecentFilesActions();
    void updateRecentFilesActions();
    void readRecentFilesFromSettings();
    void saveRecentFilesToSettings();

    void maybeOpenProgram();

private:
    void errorBox(const QString &msg, const QString &details = {});
    void warningBox(const QString &msg, const QString &details = {});
    void about();

    void openRecentFile(int idx);
    void openProgram(const QString &fileName);

private:
    Ui::MainWindow *ui;
    const unsigned short recentFilesCount = 5;
    const QString settingsFileName = QStringLiteral("CremCode.ini");
    const QString recentFilesSettingsKey = QStringLiteral("RecentFilesList");
    crem::RecentFiles m_recentFiles = crem::RecentFiles(recentFilesCount);
    std::vector<QAction *> m_recentFilesActions;
};
#endif // MAINWINDOW_H
