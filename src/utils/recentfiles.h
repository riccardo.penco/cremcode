#ifndef RECENTFILE_H
#define RECENTFILE_H

#include <QString>

#include <vector>

namespace crem {

class RecentFile
{
public:
    explicit RecentFile(const QString &fileFullPath);

    QString fileName() const;
    QString fileFullPath() const;
    bool isValid() const;

private:
    QString m_fileName;
    QString m_fileFullPath;

    friend bool operator==(const RecentFile &lhs, const RecentFile & rhs);
};

class RecentFiles
{
public:
    explicit RecentFiles(unsigned short capacity = 5);

    unsigned short capacity() const;
    void setCapacity(unsigned short capacity);

    void addFile(const QString &filePath);
    const std::vector<RecentFile> &list() const;

private:
    void truncateAtCapacity();

private:
    unsigned short m_capacity;
    std::vector<RecentFile> m_recentFiles;
};

bool operator==(const RecentFile &lhs, const RecentFile & rhs);

} //namespace crem

#endif // RECENTFILE_H
