#include "recentfiles.h"

#include <QFileInfo>

using namespace crem;
using namespace std;

RecentFile::RecentFile(const QString &fileFullPath)
{
    const auto info = QFileInfo(fileFullPath);
    if (!info.exists())
        return;

    m_fileName = info.fileName();
    m_fileFullPath = info.absoluteFilePath();
}

QString RecentFile::fileName() const
{
    return m_fileName;
}

QString RecentFile::fileFullPath() const
{
    return m_fileFullPath;
}

bool RecentFile::isValid() const
{
    return !m_fileName.isEmpty()
            && !m_fileFullPath.isEmpty();
}

RecentFiles::RecentFiles(unsigned short capacity)
    : m_capacity(capacity)
{
}

unsigned short RecentFiles::capacity() const
{
    return m_capacity;
}

void RecentFiles::setCapacity(unsigned short capacity)
{
    if (m_capacity == capacity)
        return;

    m_capacity = capacity;
}

void RecentFiles::addFile(const QString &filePath)
{
    const auto file = RecentFile(filePath);
    if (!file.isValid())
        return;

    auto it = find(std::rbegin(m_recentFiles), std::rend(m_recentFiles), file);

    if (it != rend(m_recentFiles))
    {
        // file already present in the vector, do a rotate right to make the file be the first in the vector
        rotate(it, it + 1, rend(m_recentFiles));
    }
    else
    {
        // file not present, add in the first position and truncate the vector to the desired capacity
        m_recentFiles.insert(begin(m_recentFiles), file);
        truncateAtCapacity();
    }
}

const std::vector<RecentFile> &RecentFiles::list() const
{
    return m_recentFiles;
}

void RecentFiles::truncateAtCapacity()
{
    if (m_recentFiles.size() > m_capacity)
    {
        // note:
        // cannot use the much simpler m_recentFiles.resize(m_capacity);
        // because RecentFile is not default constructible
        auto beg = begin(m_recentFiles);
        advance(beg, m_capacity);
        m_recentFiles.erase(beg, cend(m_recentFiles));
    }
}

bool crem::operator==(const RecentFile &lhs, const RecentFile &rhs)
{
    return lhs.m_fileFullPath == rhs.m_fileFullPath;
}
