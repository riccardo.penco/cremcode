#ifndef CREMCANVASS_H
#define CREMCANVASS_H

#include <QWidget>

#include "language/language.h"
#include "language/program.h"
#include "language/canvas.h"

#include <QPainter>

#include <cmath>
#include <utility>
#include <set>
#include <optional>

namespace Ui {
class CremCanvass;
}

class CremCanvas : public QWidget
{
    Q_OBJECT
    using point = std::pair<int, int>;

public:
    explicit CremCanvas(QWidget *parent = nullptr);
    ~CremCanvas();

    int boxSize() const;
    void setBoxSize(int boxSize);
    QSize maxCanvasSize() const;
//    void setDesiredSize(int width, int height);
    void setProgram(crem::Program prg);

    size_t currentLine() const;
    void setCurrentLine(size_t line);
    void step();
    void run();

Q_SIGNALS:
    void requireCanvasSize(int x, int y);
    void programCompleted();
    void runtimeError(const QString &message, const QString &details);

protected:
    void resizeEvent(QResizeEvent *event) override;
    void paintEvent(QPaintEvent *) override;

private:
    int mapX(int x) const;
    int mapY(int y) const;
    std::pair<int, int> mapXY(int x, int y) const;
    bool outOfCanvass(int cx, int cy) const;
    QColor color(crem::color col) const;
    QPixmap drawImage() const;

    void updateCanvasSize(const QSize &size);
    void drawCanvasData(QPainter &painter) const;
    void drawGrid(QPainter &painter) const;
    void drawCursor(QPainter &painter) const;

private:
    Ui::CremCanvass *ui;
    int m_boxSize = 10;
    crem::Canvas m_canvas;

};

#endif // CREMCANVASS_H
