#include "infodlg.h"
#include "ui_infodlg.h"

#include "language/language.h"

#include <QString>
#include <QTimer>

InfoDlg::InfoDlg(const QSize &maxCanvasSize, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InfoDlg),
    m_maxCanvasSize(maxCanvasSize)
{
    ui->setupUi(this);
    QTimer::singleShot(0, this, &InfoDlg::setupUi);
}

InfoDlg::~InfoDlg()
{
    delete ui;
}

void InfoDlg::setupUi()
{
    setWindowTitle(qAppName());
    ui->infoText->setReadOnly(true);
    ui->infoText->setText(infoText());

    ui->detailsText->setReadOnly(true);
    ui->detailsText->setText(detailsText());

    connect(ui->btnOk, &QPushButton::clicked, this, &InfoDlg::accept);
}

QString InfoDlg::infoText() const
{
    auto cmds = QStringList();
    for (auto cmd : crem::allCommands)
        cmds.append(crem::command_info(cmd));

    auto cols = QStringList();
    for (auto col : crem::allColors)
        cols.append(crem::color_text_pretty(col));

    const auto copyright = QStringLiteral("This program is free software: you can redistribute it and/or modify <br/>"
                                          "it under the terms of the GNU General Public License as published by <br/>"
                                          "the Free Software Foundation, either version 3 of the License, or    <br/>"
                                          "(at your option) any later version.                                  <br/>"
                                          "                                                                     <br/>"
                                          "This program is distributed in the hope that it will be useful,      <br/>"
                                          "but WITHOUT ANY WARRANTY; without even the implied warranty of       <br/>"
                                          "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        <br/>"
                                          "GNU General Public License for more details.                         <br/>"
                                          "                                                                     <br/>"
                                          "You should have received a copy of the GNU General Public License    <br/>"
                                          "along with this program.  If not, see http://www.gnu.org/licenses.   <br/>"
                                          "                                                                     <br/>"
                                          "The source code of the program is available here:                    <br/>"
                                          "https://gitlab.com/riccardo.penco/cremcode");
    auto text = QString();

    text = tr("<!DOCTYPE html><html><head><title>CremCode</title></head><body><p><b>CreCode</b>: a simple program to paint simple drawings using a simple programming language</p>"
              "<p>The commands are:</p><ul><li>%1</li></ul>"
              "<p>And these are the colors You can use:</p><ul><li>%2</li></ul><p><b>License:</b></p><p>%3</p></body></html>")
               .arg(cmds.join("</li><li>"), cols.join("</li><li>"), copyright);

    return text;
}

QString InfoDlg::detailsText() const
{
    const auto text = tr("<!DOCTYPE html><html><head><title>CremCode</title></head><body><p>This is <b>CremCode</b> version %1</p>"
                         "<p>In this computer at the current resolution and program's window size, the drawing canvas can be at most</p>"
                         "<ul><li><b>%2</b> boxes large and</li>"
                         "<li><b>%3</b> boxes high</li></ul></body></html>")
                          .arg(qApp->applicationVersion())
                          .arg(m_maxCanvasSize.width())
                          .arg(m_maxCanvasSize.height());

    return text;
}
