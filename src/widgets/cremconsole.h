#ifndef CREMCONSOLE_H
#define CREMCONSOLE_H

#include <QWidget>

#include "language/program.h"

#include <QTimer>

namespace Ui {
class CremConsole;
}

class CremConsole : public QWidget
{
    Q_OBJECT

public:
    enum class State
    {
        Paused,
        Running,
    };
    explicit CremConsole(QWidget *parent = nullptr);
    ~CremConsole();

    void setProgram(crem::Program prg);
    void setState(State state);

    void run();
    void pause();
    void step();

    QSize maxCanvasSize() const;

private:
    void setupUi();
    void setupConnections();
    void updateWidgetsState();
    void changeProgramCurrentRow();
    int timerPeriod() const;

    void onCanvasResizeRequest(int x, int y);
    void onRuntimeError(const QString &msg, const QString &details = {});
    void errorBox(const QString &msg, const QString &details = {});

private:
    Ui::CremConsole *ui;
    State m_state = State::Paused;
    QTimer m_timer;
};

#endif // CREMCONSOLE_H
