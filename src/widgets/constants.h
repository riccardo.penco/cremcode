#ifndef CONSTANTS_H
#define CONSTANTS_H

namespace crem
{

constexpr const int minBoxSize = 5;

} // namespace crem

#endif // CONSTANTS_H
