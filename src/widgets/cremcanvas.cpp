#include "cremcanvas.h"
#include "ui_cremcanvass.h"

#include "constants.h"

#include <QPainter>
#include <QPaintEngine>
#include <QPaintDevice>
#include <QResizeEvent>

CremCanvas::CremCanvas(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CremCanvass)
{
    ui->setupUi(this);

    connect(&m_canvas, &crem::Canvas::requireCanvasSize, this, &CremCanvas::requireCanvasSize);
    connect(&m_canvas, &crem::Canvas::runtimeError, this, &CremCanvas::runtimeError);
}

CremCanvas::~CremCanvas()
{
    delete ui;
}

int CremCanvas::boxSize() const
{
    return m_boxSize;
}

void CremCanvas::setBoxSize(int boxSize)
{
    if (boxSize < crem::minBoxSize
            || m_boxSize == boxSize)
        return;
    m_boxSize = boxSize;
    updateCanvasSize(size());
    update();
}

QSize CremCanvas::maxCanvasSize() const
{
    return { width() / crem::minBoxSize, height() / crem::minBoxSize };
}

//void CremCanvas::setDesiredSize(int width, int height)
//{
//    resize(width * m_boxSize + 1, height * m_boxSize + 1);
//    update();
//}

void CremCanvas::setProgram(crem::Program prg)
{
    m_canvas.setProgram(std::move(prg));
    update();
}

size_t CremCanvas::currentLine() const
{
    return m_canvas.currentLine();
}

void CremCanvas::setCurrentLine(size_t line)
{
    m_canvas.setCurrentLine(line);
    update();
}

void CremCanvas::step()
{
    m_canvas.step();
    update();
}

void CremCanvas::run()
{
    m_canvas.run();
    update();
}

void CremCanvas::resizeEvent(QResizeEvent *event)
{
    updateCanvasSize(event->size());
}

void CremCanvas::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.drawPixmap(0, 0, drawImage());
    if (m_canvas.programFinished())
        Q_EMIT programCompleted();
}

int CremCanvas::mapX(int x) const
{
    return x * m_boxSize;
}

int CremCanvas::mapY(int y) const
{
    const auto h = height();
    return h - y * m_boxSize - 1;
}

std::pair<int, int> CremCanvas::mapXY(int x, int y) const
{
    return {mapX(x), mapY(y)};
}

bool CremCanvas::outOfCanvass(int cx, int cy) const
{
    const auto w = width();
    const auto h = height();
    if (cx < 0
            || cx > w
            || cy < 0
            || cy > h)
        return true;
    return false;
}

QColor CremCanvas::color(crem::color col) const
{
    switch (col) {
    case crem::color::black         : return Qt::black;
    case crem::color::white         : return Qt::white;
    case crem::color::dark_gray     : return Qt::darkGray;
    case crem::color::gray          : return Qt::gray;
    case crem::color::light_gray    : return Qt::lightGray;
    case crem::color::red           : return Qt::red;
    case crem::color::green         : return Qt::green;
    case crem::color::blue          : return Qt::blue;
    case crem::color::cyan          : return Qt::cyan;
    case crem::color::magenta       : return Qt::magenta;
    case crem::color::yellow        : return Qt::yellow;
    case crem::color::dark_red      : return Qt::darkRed;
    case crem::color::dark_green    : return Qt::darkGreen;
    case crem::color::dark_blue     : return Qt::darkBlue;
    case crem::color::dark_cyan     : return Qt::darkCyan;
    case crem::color::dark_magenta  : return Qt::darkMagenta;
    case crem::color::dark_yellow   : return Qt::darkYellow;
    }
    return Qt::black;
}

QPixmap CremCanvas::drawImage() const
{
    auto pix = QPixmap(size());
    pix.fill(Qt::white);

    QPainter painter(&pix);
//    painter.setRenderHint(QPainter::Antialiasing, true);
    const auto w = width();
    const auto h = height();
    painter.setViewport(0, 0, w, h);
    painter.setWindow(0, 0, w, h);

    drawCanvasData(painter);
    drawGrid(painter);
    drawCursor(painter);
    return pix;
}

void CremCanvas::updateCanvasSize(const QSize &size)
{
    m_canvas.setSize(size.width() / m_boxSize, size.height() / m_boxSize);
    const auto s = maxCanvasSize();
    m_canvas.setMaxSize(s.width(), s.height());
}

void CremCanvas::drawCanvasData(QPainter &painter) const
{
    for (const auto &[p, col] : m_canvas.pixels())
    {
        const auto [cx, cy] = mapXY(p.first, p.second);
        if (outOfCanvass(cx, cy))
            continue;
        const auto brush = QBrush(color(col));
        painter.setBrush(brush);
        painter.fillRect(cx, cy, m_boxSize, -m_boxSize, brush);
    }
}

void CremCanvas::drawGrid(QPainter &painter) const
{
    const auto w = width();
    const auto h = height();
    painter.setPen(QPen(Qt::gray, 0));

    for (auto x = 0; ; ++x)
    {
        const auto cx = mapX(x);
        if (cx > w)
            break;
        painter.drawLine(cx, 0, cx, h);
    }

    for (auto y = 0; ; ++y)
    {
        const auto cy = mapY(y);
        if (cy < 0)
            break;
        painter.drawLine(0, cy, w, cy);
    }
}

void CremCanvas::drawCursor(QPainter &painter) const
{
    const auto cx = mapX(m_canvas.penState().x);
    const auto cy = mapY(m_canvas.penState().y);
    if (outOfCanvass(cx, cy))
        return;

    const auto center = QPointF(cx + m_boxSize / 2, cy - m_boxSize / 2);
    const auto r1 = static_cast<qreal>((1.0 * m_boxSize) / 4);
    const auto c1 = m_canvas.penState().pen == crem::pen::up ? color(m_canvas.penState().col).darker() : Qt::lightGray;
    const auto c2 = m_canvas.penState().pen == crem::pen::up ? Qt::white : color(m_canvas.penState().col);
    painter.save();
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen(QPen(c1, 2));
    painter.setBrush(c2);
    painter.drawEllipse(center, r1, r1);
    painter.restore();
}
