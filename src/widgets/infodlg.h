#ifndef INFODLG_H
#define INFODLG_H

#include <QDialog>

namespace Ui {
class InfoDlg;
}

class InfoDlg : public QDialog
{
    Q_OBJECT

public:
    explicit InfoDlg(const QSize &maxCanvasSize, QWidget *parent = nullptr);
    ~InfoDlg();

private:
    void setupUi();
    QString infoText() const;
    QString detailsText() const;

private:
    Ui::InfoDlg *ui;
    QSize m_maxCanvasSize;
};

#endif // INFODLG_H
