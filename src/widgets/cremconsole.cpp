#include "cremconsole.h"
#include "ui_cremconsole.h"

#include "constants.h"
#include "language/writer.h"

#include <QMessageBox>

CremConsole::CremConsole(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CremConsole)
{
    ui->setupUi(this);

    QTimer::singleShot(0, this, &CremConsole::setupUi);
}

CremConsole::~CremConsole()
{
    delete ui;
}

void CremConsole::setProgram(crem::Program prg)
{
    ui->listing->clear();
    const auto writer = crem::Writer();
    ui->listing->insertItems(0, writer.printAsLines(prg));
    ui->canvas->setProgram(std::move(prg));
}

void CremConsole::setState(CremConsole::State state)
{
    m_state = state;
    updateWidgetsState();
}

void CremConsole::run()
{
    m_timer.setInterval(timerPeriod());
    m_timer.start();
    m_state = State::Running;
    updateWidgetsState();
}

void CremConsole::pause()
{
    m_timer.stop();
    m_state = State::Paused;
    updateWidgetsState();
}

void CremConsole::step()
{
    ui->canvas->step();

    ui->listing->setCurrentRow(static_cast<int>(ui->canvas->currentLine()) - 1);
    ui->listing->scrollTo(ui->listing->currentIndex());
}

QSize CremConsole::maxCanvasSize() const
{
    return ui->canvas->maxCanvasSize();
}

void CremConsole::setupUi()
{
    ui->listing->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
    ui->listing->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);

    ui->boxSize->setMinimum(crem::minBoxSize);
    ui->boxSize->setValue(20);
    setupConnections();
    ui->canvas->setBoxSize(ui->boxSize->value());

    updateWidgetsState();
}

void CremConsole::setupConnections()
{
    connect(ui->btnRun, &QPushButton::clicked, this, &CremConsole::run);
    connect(ui->btnPause, &QPushButton::clicked, this, &CremConsole::pause);
    connect(ui->btnStep, &QPushButton::clicked, this, &CremConsole::step);

    connect(ui->canvas, &CremCanvas::requireCanvasSize, this, &CremConsole::onCanvasResizeRequest);
    connect(ui->canvas, &CremCanvas::programCompleted, this, &CremConsole::pause);
    connect(ui->canvas, &CremCanvas::runtimeError, this, &CremConsole::onRuntimeError);

    connect(ui->speed, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, [this](int){ m_timer.setInterval(timerPeriod()); });
    connect(ui->boxSize, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, [this](int size){ ui->canvas->setBoxSize(size); });

    connect(ui->listing->selectionModel(), &QItemSelectionModel::currentRowChanged, this, &CremConsole::changeProgramCurrentRow);

    connect(&m_timer, &QTimer::timeout, this, &CremConsole::step);
}

void CremConsole::updateWidgetsState()
{
    ui->btnRun->setChecked(m_state == State::Running);
    ui->btnPause->setChecked(m_state == State::Paused);
    ui->btnStep->setEnabled(m_state == State::Paused);
    ui->listing->setEnabled(m_state == State::Paused);
}

void CremConsole::changeProgramCurrentRow()
{
    if (m_state == State::Running)
        return;
    ui->canvas->setCurrentLine(ui->listing->currentRow() + 1);
}

int CremConsole::timerPeriod() const
{
    return 1000 / ui->speed->value();
}

void CremConsole::onCanvasResizeRequest(int x, int y)
{
    if (x <= 1
            || y <= 1)
    {
        errorBox(tr("Cannot set a dimension less than 1"));
        return;
    }
    const auto s = ui->canvas->size();
    const auto minS = ui->boxSize->minimum();
    const auto maxS = ui->boxSize->maximum();
    const auto boxX = std::clamp(s.width() / x, minS, maxS);
    const auto boxY = std::clamp(s.height() / x, minS, maxS);
    ui->boxSize->setValue(std::min(boxX, boxY));
}

void CremConsole::onRuntimeError(const QString &msg, const QString &details)
{
    pause();
    errorBox(msg, details);
}

void CremConsole::errorBox(const QString &msg, const QString &details)
{
    QMessageBox box;
    box.setIcon(QMessageBox::Critical);
    box.setText(msg);
    if (!details.isEmpty())
        box.setDetailedText(details);
    box.setStandardButtons(QMessageBox::Ok);
    box.setDefaultButton(QMessageBox::Ok);
    box.exec();

}
