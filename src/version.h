#ifndef VERSION_H

#define VERSION_H
#define VER_FILEVERSION             0,0,1,0
#define VER_FILEVERSION_STR         "0.0.1.0\0"

#define VER_PRODUCTVERSION          VER_FILEVERSION
#define VER_PRODUCTVERSION_STR      "00.00.01.01\0"

#define VER_COMPANYNAME_STR         "RikiSisa"
#define VER_FILEDESCRIPTION_STR     "Programma per semplici disegni programmando"
#define VER_INTERNALNAME_STR        "CremCode"
#define VER_LEGALCOPYRIGHT_STR      "Copyright 2020 RikiSisa"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "cremcode.exe"
#define VER_PRODUCTNAME_STR         "cremcode"

#define VER_COMPANYDOMAIN_STR       "https://gitlab.com/riccardo.penco/cremcode"

#endif // VERSION_H
