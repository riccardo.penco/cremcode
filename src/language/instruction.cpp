#include "instruction.h"

#include "utils.h"

#include <algorithm>

using namespace crem;

Instruction::Instruction(command cmd) :
    m_cmd(cmd)
{
}

Instruction::Instruction(command cmd, int qty) :
    m_cmd(cmd),
    m_qty(qty)
{
}

Instruction::Instruction(command cmd, int x, int y) :
    m_cmd(cmd),
    m_x(x),
    m_y(y)
{
}

Instruction::Instruction(command cmd, crem::color col) :
    m_cmd(cmd),
    m_color(col)
{
}

bool Instruction::isValid() const noexcept
{
    if (!m_isValid.has_value())
        check();
    return *m_isValid;
}

command Instruction::cmd() const
{
    return m_cmd;
}

std::optional<int> Instruction::qty() const
{
    return m_qty;
}

std::optional<int> Instruction::x() const

{
    return m_x;
}

std::optional<int> Instruction::y() const
{
    return m_y;
}

std::optional<color> Instruction::color() const
{
    return m_color;
}

void Instruction::check() const
{
    if (utils::contained(m_cmd, commandsNoArgs))
    {
        if (m_qty.has_value()
            || m_x.has_value()
            || m_y.has_value()
            || m_color.has_value())
            m_isValid = false;
    }
    else if (utils::contained(m_cmd, commandsQtyArg))
    {
        if (!m_qty.has_value()
            || m_x.has_value()
            || m_y.has_value()
            || m_color.has_value())
            m_isValid = false;
    }
    else if (utils::contained(m_cmd, commandsXYArgs))
    {
        if (m_qty.has_value()
            || !m_x.has_value()
            || !m_y.has_value()
            || m_color.has_value())
            m_isValid = false;
    }
    else if (utils::contained(m_cmd, commandsColorArg))
    {
        if (m_qty.has_value()
            || m_x.has_value()
            || m_y.has_value()
            || !m_color.has_value())
            m_isValid = false;
    }
//    else if (utils::contained(m_cmd, commandsPenArg))
//    {
//        if (m_qty.has_value()
//            || m_x.has_value()
//            || m_y.has_value()
//            || m_color.has_value()
//            || !m_pen.has_value())
//            m_isValid = false;
//    }
    m_isValid = true;
}
