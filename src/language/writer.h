#ifndef WRITER_H
#define WRITER_H

#include "language.h"
#include "instruction.h"
#include "program.h"

#include <QString>

namespace crem {

class Writer
{
public:
    QString print(const Program &prg) const;
    QStringList printAsLines(const Program &prg) const;

private:
    QString prettyPrint(const Instruction &instr) const;
};

} //namespace crem

#endif // WRITER_H
