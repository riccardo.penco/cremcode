#include "canvas.h"

#include "floodfiller.h"

#include <stdexcept>

using namespace crem;

Canvas::Canvas(QObject *parent)
    : QObject(parent)
{
}

void Canvas::setSize(int width, int height)
{
    if (width < 5
            || height < 5)
        return;
    if (m_width == width
            && m_height == height)
        return;
    if (m_width < m_sizeX)
        throw std::runtime_error(tr("Canvas X size would be smaller than the required %1 value")
                                     .arg(m_sizeX)
                                     .toStdString());
    if (m_height < m_sizeY)
        throw std::runtime_error(tr("Canvas Y size would be smaller than the required %1 value")
                                     .arg(m_sizeY)
                                     .toStdString());
    m_width = width;
    m_height = height;
    update();
}

void Canvas::setMaxSize(int width, int height)
{
    m_maxWidth = width;
    m_maxHeight = height;
}

void Canvas::setProgram(Program prg)
{
    m_program = std::move(prg);
    m_runTo = 0;
    update();
}

size_t Canvas::currentLine() const
{
    return m_runTo;
}

void Canvas::setCurrentLine(size_t line)
{
    if (m_runTo == line
            || m_program.size() == 0
            || line > m_program.size())
        return;
    m_runTo = line;
    update();
}

std::optional<Instruction> Canvas::currentInstruction() const
{
    if (m_runTo == 0
            || m_program.size() == 0)
        return std::nullopt;

    return m_program.instructions().at(m_runTo - 1);
}

std::optional<Instruction> Canvas::nextInstruction() const
{
    const auto size = m_program.size();
    if (m_runTo == size
            || size == 0)
        return std::nullopt;

    return m_program.instructions().at(m_runTo);
}

void Canvas::step()
{
    if (m_runTo < m_program.size())
        ++m_runTo;
    update();
}

void Canvas::run()
{
    m_runTo = m_program.size();
    update();
}

const PenState &Canvas::penState() const
{
    return m_penState;
}

const std::map<Canvas::point, color> &Canvas::pixels() const
{
    return m_pixels;
}

bool Canvas::programFinished() const
{
    return m_runTo == m_program.size();
}

bool Canvas::outOfCanvass(int x, int y) const
{
    return x < 0
           || x > m_width
           || y < 0
           || y > m_height;
}

void Canvas::update()
{
    m_pixels.clear();
    m_penState = PenState();
    auto line = 0ul;
    try
    {
        for (line = 0; line < m_runTo; ++line)
            performInstruction(m_penState, m_program.instructions().at(line));

    }
    catch (const std::exception &ex)
    {
        m_runTo = line;
        Q_EMIT runtimeError(tr("Runtime error at line %1").arg(line + 1),
                            tr("Details: %2").arg(ex.what()));
    }
}

void Canvas::performInstruction(PenState &state, const Instruction &inst)
{
    if (!inst.isValid())
        return;
    switch (inst.cmd()) {
    case crem::command::set_size:   performSetSize(*inst.x(), *inst.y());                       break;
    case crem::command::pen_up:     performPenUp(state);                                        break;
    case crem::command::pen_down:   performPenDown(state);                                      break;
    case crem::command::move_left:  performMoveLeft(state, inst.qty().value());                 break;
    case crem::command::move_right: performMoveRight(state, inst.qty().value());                break;
    case crem::command::move_up:    performMoveUp(state, inst.qty().value());                   break;
    case crem::command::move_down:  performMoveDown(state, inst.qty().value());                 break;
    case crem::command::move_to:    performMoveTo(state, inst.x().value(), inst.y().value());   break;
    case crem::command::set_color:  performSetColor(state, inst.color().value());               break;
    case crem::command::fill:       performFill(state);                                         break;
    }
}

void Canvas::performSetSize(int x, int y)
{
    Q_EMIT requireCanvasSize(x, y);
    if (x > m_maxWidth)
        throw std::runtime_error(tr("Canvas cannot be larger than %1 boxes")
                                     .arg(m_maxWidth)
                                     .toStdString());
    if (y > m_maxHeight)
        throw std::runtime_error(tr("Canvas cannot be higher than %1 boxes")
                                     .arg(m_maxHeight)
                                     .toStdString());

    m_sizeX = x;
    m_sizeY = y;

    for (auto cx = m_sizeX; cx <= m_width; ++cx)
        for (auto cy = 0; cy <= m_height; ++cy)
            m_pixels[{cx, cy}] = color::light_gray;

    for (auto cy = m_sizeY; cy <= m_height; ++cy)
        for (auto cx = 0; cx <= m_width; ++cx)
            m_pixels[{cx, cy}] = color::light_gray;
}

void Canvas::performPenUp(PenState &state)
{
    state.pen = pen::up;
}

void Canvas::performPenDown(PenState &state)
{
    state.pen = pen::down;
}

void Canvas::performMoveLeft(PenState &state, int qty)
{
    performMoveHor(state, -qty);
}

void Canvas::performMoveRight(PenState &state, int qty)
{
    performMoveHor(state, qty);
}

void Canvas::performMoveUp(PenState &state, int qty)
{
    performMoveVer(state, qty);
}

void Canvas::performMoveDown(PenState &state, int qty)
{
    performMoveVer(state, -qty);
}

void Canvas::performMoveTo(PenState &state, int x, int y)
{
    if (outOfCanvass(x, y))
        return;

    if (state.pen == pen::up)
    {
        state.x = x;
        state.y = y;
        return;
    }
    performMoveToPaint(state, x, y);
}

void Canvas::performSetColor(PenState &state, color col)
{
    state.col = col;
}

void Canvas::performFill(PenState &state)
{
    auto filler = FloodFiller();
    const auto pixels = filler.pointsToColor(*this, state.x, state.y);
    for (const auto &p : pixels)
        m_pixels[p] = state.col;
}

void Canvas::maybePaint(PenState &state)
{
    if (state.pen == crem::pen::up)
        return;
    if (outOfCanvass(state.x, state.y))
        return;
    m_pixels[{state.x, state.y}] = state.col;
}

void Canvas::performMoveHor(PenState &state, int qty)
{
    // performMoveXXX(state, 0) should paint at the current location
    maybePaint(state);

    const auto step = qty > 0 ? 1 : -1;
    for (; qty != 0; qty -= step)
    {
        state.x += step;
        if (state.x < 0
                || state.x > m_width)
            return;
        maybePaint(state);
    }
}

void Canvas::performMoveVer(PenState &state, int qty)
{
    // performMoveXXX(state, 0) should paint at the current location
    maybePaint(state);

    const auto step = qty > 0 ? 1 : -1;
    for (; qty != 0; qty -= step)
    {
        state.y += step;
        if (state.y < 0
            || state.y > m_width)
            return;
        maybePaint(state);
    }
}

void Canvas::performMoveToPaint(PenState &state, int x, int y)
{
    if (state.x == x)
    {
        performMoveToPaintVert(state, y);
        return;
    }

    // performMoveXXX(state, 0) should paint at the current location
    maybePaint(state);

    const auto dx = std::abs(x - state.x);
    const auto dy = std::abs(y - state.y);
    if (dx >= dy)
        performMoveToPaintXby1(state, x, y);
    else
        performMoveToPaintYby1(state, x, y);
}

void Canvas::performMoveToPaintVert(PenState &state, int y)
{
    performMoveVer(state, y - state.y);
}

void Canvas::performMoveToPaintXby1(PenState &state, int x, int y)
{
    // performMoveXXX(state, 0) should paint at the current location
    maybePaint(state);

    auto dx = x - state.x;
    const auto dy = y - state.y;
    const auto tan = static_cast<double>(dy) / dx;
    const auto fromY = state.y;
    auto xMove = 0;
    const auto step = dx > 0 ? 1 : -1;

    for (; dx != 0; dx -= step)
    {
        state.x += step;
        xMove += step;
        state.y = static_cast<int>(fromY + xMove * tan);
        if (outOfCanvass(state.x, state.y))
            return;
        maybePaint(state);
    }

    // make sure the destination point is painted
    state.x = x;
    state.y = y;
    maybePaint(state);
}

void Canvas::performMoveToPaintYby1(PenState &state, int x, int y)
{
    // performMoveXXX(state, 0) should paint at the current location
    maybePaint(state);

    const auto dx = x - state.x;
    auto dy = y - state.y;
    const auto tan = static_cast<double>(dx) / dy;
    const auto fromX = state.x;
    auto yMove = 0;
    const auto step = dy > 0 ? 1 : -1;

    for (; dy != 0; dy -= step)
    {
        state.y += step;
        yMove += step;
        state.x = static_cast<int>(fromX + yMove * tan);
        if (outOfCanvass(state.x, state.y))
            return;
        maybePaint(state);
    }

    // make sure the destination point is painted
    state.x = x;
    state.y = y;
    maybePaint(state);
}
