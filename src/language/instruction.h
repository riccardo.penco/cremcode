#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include "language.h"

#include <optional>

namespace crem {

class Instruction
{
public:
    explicit Instruction(crem::command cmd);
    explicit Instruction(crem::command cmd, int qty);
    explicit Instruction(crem::command cmd, int x, int y);
    explicit Instruction(crem::command cmd, crem::color col);

    Instruction(const Instruction &) = default;
    Instruction(Instruction &&) = default;

    ~Instruction() = default;

    Instruction &operator=(const Instruction &) = default;
    Instruction &operator=(Instruction &&) = default;

    bool isValid() const noexcept;

    command cmd() const;

    std::optional<int> qty() const;

    std::optional<int> x() const;

    std::optional<int> y() const;

    std::optional<crem::color> color() const;

private:
    void check() const;

private:
    mutable std::optional<bool> m_isValid;
    crem::command m_cmd;
    std::optional<int> m_qty;
    std::optional<int> m_x;
    std::optional<int> m_y;
    std::optional<crem::color> m_color;
};

} //namespace crem

#endif // INSTRUCTION_H
