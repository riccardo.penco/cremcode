#ifndef PARSER_H
#define PARSER_H

#include "language.h"
#include "instruction.h"
#include "program.h"

#include <QString>

#include <utility>

namespace crem {

struct ParseResult
{
    Program program;
    QStringList errors;
};

class Parser
{
public:
    ParseResult parseProgram(const QString &code) const;

private:
    Instruction tryParseLine(const QString &line) const;

    command tryParseCommand(QString &text) const;
    int tryParseQty(const QString &text) const;
    color tryParseColor(const QString &text) const;

    Instruction tryParseQtyInstruction(command cmd, const QString &text) const;
    Instruction tryParseXYInstruction(command cmd, const QString &text) const;
    Instruction tryParseColorInstruction(command cmd, const QString &text) const;
};

} //namespace crem

#endif // PARSER_H
