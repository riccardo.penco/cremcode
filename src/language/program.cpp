#include "program.h"

using namespace crem;


bool Program::appendInstrunction(Instruction inst)
{
    if (inst.isValid())
    {
        m_instructions.push_back(std::move(inst));
        return true;
    }
    return false;
}

size_t Program::size() const
{
    return m_instructions.size();
}

const std::vector<Instruction> &Program::instructions() const
{
    return m_instructions;
}
