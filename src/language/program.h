#ifndef PROGRAM_H
#define PROGRAM_H

#include "language.h"
#include "instruction.h"

#include <vector>

namespace crem {

class Program
{
public:
    bool appendInstrunction(Instruction inst);
    size_t size() const;
    const std::vector<Instruction> &instructions() const;

private:
    std::vector<Instruction> m_instructions;
};

} //namespace crem

#endif // PROGRAM_H
