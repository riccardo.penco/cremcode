#ifndef FLOODFILLER_H
#define FLOODFILLER_H

#include "language.h"
#include "canvas.h"

#include <utility>
#include <map>
#include <set>

namespace crem {

// source: https://www.learnpyqt.com/blog/implementing-qpainter-flood-fill-pyqt5pyside/
class FloodFiller
{
    using point = std::pair<int, int>;
public:
    std::set<point> pointsToColor(const Canvas &canvas, int x, int y);

private:
//    color getColor(int x, int y) const;
    color getColor(const Canvas &canvas, const point &p) const;
    bool isToFill(const Canvas &canvas, const point &p) const;
    point upper(point p) const;
    point left(point p) const;
    point lower(point p) const;
    point right(point p) const;
    void maybeAddToQueye(const Canvas &canvas, point p);
    void processPoint(const Canvas &canvas, const std::pair<int, int> &p);
    void processQueue(const Canvas &canvas);

private:
    color m_color;
    std::set<std::pair<int, int>> m_queue;
    std::set<std::pair<int, int>> m_seen;
    std::set<std::pair<int, int>> m_toFill;
};

} //namespace crem

#endif // FLOODFILLER_H
