#include "writer.h"

#include "utils.h"

#include <QCoreApplication>

using namespace crem;

QString Writer::print(const Program &prg) const
{
    return printAsLines(prg).join("\n");
}

QStringList Writer::printAsLines(const Program &prg) const
{
    auto lines = QStringList();

    for (const auto &instr : prg.instructions())
    {
        lines.append(prettyPrint(instr));
    }
    return lines;
}

QString Writer::prettyPrint(const Instruction &instr) const
{
    const auto cmd = instr.cmd();

    if (utils::contained(cmd, commandsNoArgs))
        return command_text_pretty(cmd);

    if (utils::contained(cmd, commandsQtyArg))
        return QString("%1 %2")
            .arg(command_text_pretty(cmd))
            .arg(*instr.qty());

    if (utils::contained(cmd, commandsXYArgs))
        return QString("%1 %2 %3")
            .arg(command_text_pretty(cmd))
            .arg(*instr.x())
            .arg(*instr.y());

    if (utils::contained(cmd, commandsColorArg))
        return QString("%1 %2")
            .arg(command_text_pretty(cmd),
                 color_text_pretty(*instr.color()));

    return QCoreApplication::translate("Writer", "unexpected instruction");
}
