#ifndef UTILS_H
#define UTILS_H

#include "language.h"

#include <optional>
#include <vector>
#include <map>

namespace crem::utils
{
bool contained(crem::command cmd, const std::vector<command> &commands);

template<typename T>
std::optional<T> findInMap(const std::map<QString, T> tMap, const QString &txt)
{
    const auto it = tMap.find(txt);
    return it == tMap.cend() ? std::nullopt : std::optional(it->second);
}

template<typename T>
T tryFindInMap(const std::map<QString, T> tMap, const QString &txt)
{
    const auto it = tMap.find(txt);
    if (it == tMap.cend())
        throw std::runtime_error("key not found in the map");

    return it->second;
}

} //namespace crem::utils

#endif // UTILS_H
