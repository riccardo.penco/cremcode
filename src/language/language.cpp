#include "language.h"

#include <QCoreApplication>

QString crem::command_text_pretty(crem::command cmd)
{
    switch (cmd) {
    case command::set_size:     return QCoreApplication::translate("crem", "Set SIZE");
    case command::pen_up:       return QCoreApplication::translate("crem", "Pen UP");
    case command::pen_down:     return QCoreApplication::translate("crem", "Pen DOWN");
    case command::move_left:    return QCoreApplication::translate("crem", "Move LEFT");
    case command::move_right:   return QCoreApplication::translate("crem", "Move RIGHT");
    case command::move_up:      return QCoreApplication::translate("crem", "Move UP");
    case command::move_down:    return QCoreApplication::translate("crem", "Move DOWN");
    case command::move_to:      return QCoreApplication::translate("crem", "Move TO");
    case command::set_color:    return QCoreApplication::translate("crem", "Set COLOR");
    case command::fill:         return QCoreApplication::translate("crem", "Fill");
    }
    return {};
}

QString crem::color_text_pretty(crem::color col)
{
    switch (col)
    {
    case color::black:           return QCoreApplication::translate("cream", "Black");
    case color::white:           return QCoreApplication::translate("cream", "White");
    case color::dark_gray:       return QCoreApplication::translate("cream", "Dark Gray");
    case color::gray:            return QCoreApplication::translate("cream", "Gray");
    case color::light_gray:      return QCoreApplication::translate("cream", "Light Gray");
    case color::red:             return QCoreApplication::translate("cream", "Red");
    case color::green:           return QCoreApplication::translate("cream", "Green");
    case color::blue:            return QCoreApplication::translate("cream", "Blue");
    case color::cyan:            return QCoreApplication::translate("cream", "Cyan");
    case color::magenta:         return QCoreApplication::translate("cream", "Magenta");
    case color::yellow:          return QCoreApplication::translate("cream", "Yellow");
    case color::dark_red:        return QCoreApplication::translate("cream", "Dark Red");
    case color::dark_green:      return QCoreApplication::translate("cream", "Dark Green");
    case color::dark_blue:       return QCoreApplication::translate("cream", "Dark Blue");
    case color::dark_cyan:       return QCoreApplication::translate("cream", "Dark Cyan");
    case color::dark_magenta:    return QCoreApplication::translate("cream", "Dark Magenta");
    case color::dark_yellow:     return QCoreApplication::translate("cream", "Dark Yellow");
    }
    return {};
}

QString crem::command_syntax(crem::command cmd)
{
    const auto txt = command_text_pretty(cmd);

    switch (cmd) {
    case command::set_size:     return QCoreApplication::translate("crem", "%1 X Y").arg(txt);
    case command::pen_up:       return QCoreApplication::translate("crem", "%1    ").arg(txt);
    case command::pen_down:     return QCoreApplication::translate("crem", "%1    ").arg(txt);
    case command::move_left:    return QCoreApplication::translate("crem", "%1 N  ").arg(txt);
    case command::move_right:   return QCoreApplication::translate("crem", "%1 N  ").arg(txt);
    case command::move_up:      return QCoreApplication::translate("crem", "%1 N  ").arg(txt);
    case command::move_down:    return QCoreApplication::translate("crem", "%1 N  ").arg(txt);
    case command::move_to:      return QCoreApplication::translate("crem", "%1 X Y").arg(txt);
    case command::set_color:    return QCoreApplication::translate("crem", "%1 C  ").arg(txt);
    case command::fill:         return QCoreApplication::translate("crem", "%1    ").arg(txt);
    }
    return {};
}

QString crem::command_info(crem::command cmd)
{
    const auto txt = command_syntax(cmd);

    switch (cmd) {
    case command::set_size:     return QCoreApplication::translate("crem", "%1 - sets the canvas' size to X, Y").arg(txt);
    case command::pen_up:       return QCoreApplication::translate("crem", "%1 - lifts the pen").arg(txt);
    case command::pen_down:     return QCoreApplication::translate("crem", "%1 - lowers the pen").arg(txt);
    case command::move_left:    return QCoreApplication::translate("crem", "%1 - moves the cursor left by N units, painting in the current color if the pen is down").arg(txt);
    case command::move_right:   return QCoreApplication::translate("crem", "%1 - moves the cursor right by N units, painting in the current color if the pen is down").arg(txt);
    case command::move_up:      return QCoreApplication::translate("crem", "%1 - moves the cursor up by N units, painting in the current color if the pen is down").arg(txt);
    case command::move_down:    return QCoreApplication::translate("crem", "%1 - moves the cursor down by N units, painting in the current color if the pen is down").arg(txt);
    case command::move_to:      return QCoreApplication::translate("crem", "%1 - moves the cursor to the position specified by X and Y coordinates, painting in the current color if the pen is down").arg(txt);
    case command::set_color:    return QCoreApplication::translate("crem", "%1 - sets C as the painting color").arg(txt);
    case command::fill:         return QCoreApplication::translate("crem", "%1 - fills the region around the cursor with the current color").arg(txt);
    }
    return {};
}

int crem::command_words(crem::command cmd)
{
    return command_text_pretty(cmd).simplified().split(QStringLiteral(" "), Qt::SkipEmptyParts).size();
}

int crem::color_words(crem::color col)
{
    return color_text_pretty(col).simplified().split(QStringLiteral(" "), Qt::SkipEmptyParts).size();
}
