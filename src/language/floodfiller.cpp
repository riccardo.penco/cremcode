#include "floodfiller.h"

using namespace crem;

//color FloodFiller::getColor(int x, int y) const
//{
//    return getColor({x, y});
//}

std::set<FloodFiller::point> FloodFiller::pointsToColor(const Canvas &canvas, int x, int y)
{
    if (canvas.outOfCanvass(x, y))
        return {};
    m_color = getColor(canvas, {x, y});
    m_queue.insert({x, y});
    processQueue(canvas);
    return m_toFill;
}

color FloodFiller::getColor(const Canvas &canvas, const FloodFiller::point &p) const
{
    const auto it = canvas.m_pixels.find(p);
    if (it == canvas.m_pixels.cend())
        return color::white;

    return it->second;
}

bool FloodFiller::isToFill(const Canvas &canvas, const FloodFiller::point &p) const
{
    return getColor(canvas, p) == m_color;
}

FloodFiller::point FloodFiller::upper(FloodFiller::point p) const
{
    ++p.second;
    return p;
}

FloodFiller::point FloodFiller::left(FloodFiller::point p) const
{
    --p.first;
    return p;
}

FloodFiller::point FloodFiller::lower(FloodFiller::point p) const
{
    --p.second;
    return p;
}

FloodFiller::point FloodFiller::right(FloodFiller::point p) const
{
    ++p.first;
    return p;
}

void FloodFiller::maybeAddToQueye(const Canvas &canvas, FloodFiller::point p)
{
    if (m_seen.find(p) != m_seen.cend())
        return;
    if (canvas.outOfCanvass(p.first, p.second))
        return;
    m_queue.insert(std::move(p));
}

void FloodFiller::processPoint(const Canvas &canvas, const std::pair<int, int> &p)
{
    const auto [it, inserted] = m_seen.insert(p);
    if (!inserted)
        return;
    if (canvas.outOfCanvass(p.first, p.second))
        return;

    if (!isToFill(canvas, p))
        return;

    m_toFill.insert(p);
    maybeAddToQueye(canvas, upper(p));
    maybeAddToQueye(canvas, left(p));
    maybeAddToQueye(canvas, lower(p));
    maybeAddToQueye(canvas, right(p));
}

void FloodFiller::processQueue(const Canvas &canvas)
{
    while (!m_queue.empty())
    {
        auto it = m_queue.begin();
        const auto pt = *it;
        m_queue.erase(it);
        processPoint(canvas, pt);
    }
}
