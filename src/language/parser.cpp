#include "parser.h"

#include "utils.h"

#include <QCoreApplication>

#include <stdexcept>

using namespace crem;

ParseResult Parser::parseProgram(const QString &code) const
{
    const auto lines = code.split("\n");
    ParseResult result;
    const auto numLines = lines.size();
    for (auto n = 0; n < numLines; ++n)
    {
        const auto line = lines.at(n).simplified();
        if (line.isEmpty())
            continue;
        try
        {
            auto instr = tryParseLine(line);
            if (instr.isValid())
                result.program.appendInstrunction(instr);
            else
                result.errors.append(QCoreApplication::translate("Parser", "line %1: instruction is not valid")
                                     .arg(n + 1));
        }
        catch (const std::exception &ex)
        {
            result.errors.append(ex.what());
        }
    }
    return result;
}

Instruction Parser::tryParseLine(const QString &line) const
{
    // split the line using the # delimiter and consider only the first part
    // to remove the comments
    auto text = line.simplified().toLower().split(QStringLiteral("#"), Qt::KeepEmptyParts).at(0);

    // the line was empty or a comment
    if (text.isEmpty())
        throw std::runtime_error(QCoreApplication::translate("Parser", "empty line").toStdString());

    const auto cmd = tryParseCommand(text);

    const auto parts = text.split(QStringLiteral(" "), Qt::SkipEmptyParts);


    if (utils::contained(cmd, commandsNoArgs))
        return Instruction(cmd);

    if (utils::contained(cmd, commandsQtyArg))
        return tryParseQtyInstruction(cmd, text);

    if (utils::contained(cmd, commandsXYArgs))
        return tryParseXYInstruction(cmd, text);

    if (utils::contained(cmd, commandsColorArg))
        return tryParseColorInstruction(cmd, text);

    throw std::runtime_error(QCoreApplication::translate("Parser", "impossible to parse the command type in %1")
                             .arg(text)
                             .toStdString());
}

command Parser::tryParseCommand(QString &text) const
{
    for (auto [txt, cmd] : m_textToCommand)
    {
        if (text.startsWith(txt))
        {
            text = text.remove(0, txt.length()).simplified();
            return cmd;
        }
    }
    throw std::runtime_error(QCoreApplication::translate("Parser", "command not found in %1")
                             .arg(text)
                             .toStdString());
}

int Parser::tryParseQty(const QString &text) const
{
    auto ok = false;
    const auto qty = text.toInt(&ok);
    if (!ok)
        throw std::runtime_error(QCoreApplication::translate("Parser", "param %1 cannot be converted to int")
                                 .arg(text)
                                 .toStdString());

    return qty;
}

color Parser::tryParseColor(const QString &text) const
{
    return utils::tryFindInMap(m_textToColor, text);
}

Instruction Parser::tryParseQtyInstruction(command cmd, const QString &text) const
{
    const auto qty = tryParseQty(text);
    return Instruction(cmd, qty);
}

Instruction Parser::tryParseXYInstruction(command cmd, const QString &text) const
{
    const auto parts = text.simplified().split(" ", Qt::SkipEmptyParts);
    if (parts.size() < 2)
        throw std::runtime_error(QCoreApplication::translate("Parser", "not enough params for the command").toStdString());
    if (parts.size() > 2)
        throw std::runtime_error(QCoreApplication::translate("Parser", "too many params for the command").toStdString());

    const auto x = tryParseQty(parts.at(0));
    const auto y = tryParseQty(parts.at(1));
    return Instruction(cmd, x, y);
}

Instruction Parser::tryParseColorInstruction(command cmd, const QString &text) const
{
    for (const auto &[txt, col] : m_textToColor)
    {
        if (text.startsWith(txt))
            return Instruction(cmd, col);
    }
    throw std::runtime_error(QCoreApplication::translate("Parser", "color not found in %1")
                                 .arg(text)
                                 .toStdString());
}

