#ifndef CANVAS_H
#define CANVAS_H

#include "language.h"
#include "instruction.h"
#include "program.h"

#include <QObject>

#include <map>
#include <utility>
#include <optional>

namespace crem {

struct PenState
{
    int x = 0;
    int y = 0;
    color col = color::white;
    crem::pen pen = crem::pen::up;
};

class Canvas : public QObject
{
    Q_OBJECT
    using point = std::pair<int, int>;
    friend class FloodFiller;
public:
    explicit Canvas(QObject *parent = nullptr);

    void setSize(int width, int height);
    void setMaxSize(int width, int height);
    void setProgram(Program prg);

    size_t currentLine() const;
    void setCurrentLine(size_t line);
    std::optional<Instruction> currentInstruction() const;
    std::optional<Instruction> nextInstruction() const;
    void step();
    void run();

    const PenState &penState() const;
    const std::map<point, color> &pixels() const;
    bool programFinished() const;

Q_SIGNALS:
    void requireCanvasSize(int x, int y);
    void runtimeError(const QString &message, const QString &details);

private:
    bool outOfCanvass(int x, int y) const;
    void update();
    void performInstruction(PenState &state, const Instruction &inst);
    void performSetSize(int x, int y);
    void performPenUp(PenState &state);
    void performPenDown(PenState &state);
    void performMoveLeft(PenState &state, int qty);
    void performMoveRight(PenState &state, int qty);
    void performMoveUp(PenState &state, int qty);
    void performMoveDown(PenState &state, int qty);
    void performMoveTo(PenState &state, int x, int y);
    void performSetColor(PenState &state, color col);
    void performFill(PenState &state);
    void maybePaint(PenState &state);
    void performMoveHor(PenState &state, int qty);
    void performMoveVer(PenState &state, int qty);
    void performMoveToPaint(PenState &state, int x, int y);
    void performMoveToPaintVert(PenState &state, int y);
    void performMoveToPaintXby1(PenState &state, int x, int y);
    void performMoveToPaintYby1(PenState &state, int x, int y);

private:
    int m_width = 20;
    int m_height = 15;
    int m_maxWidth = 20;
    int m_maxHeight = 15;
    int m_sizeX = -1;
    int m_sizeY = -1;
    Program m_program;
    size_t m_runTo = 0;
    PenState m_penState;
    std::map<point, color> m_pixels;
};

} //namespace crem

#endif // CANVAS_H
