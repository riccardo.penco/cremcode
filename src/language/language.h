#ifndef LANGUAGE_H
#define LANGUAGE_H

#include <QString>
#include <QColor>

#include <vector>
#include <map>

namespace crem {

    enum class command
    {
        set_size,
        pen_up,
        pen_down,
        move_left,
        move_right,
        move_up,
        move_down,
        move_to,
        set_color,
        fill,
    };

    enum class color
    {
        black,
        white,
        dark_gray,
        gray,
        light_gray,
        red,
        green,
        blue,
        cyan,
        magenta,
        yellow,
        dark_red,
        dark_green,
        dark_blue,
        dark_cyan,
        dark_magenta,
        dark_yellow,
    };

    enum class pen
    {
        up,
        down,
    };

    QString command_text_pretty(command cmd);
    QString color_text_pretty(color col);
    QString pen_text_pretty(command cmd);
    QString command_syntax(command cmd);
    QString command_info(command cmd);
    int command_words(command cmd);
    int color_words(color col);

    const std::vector<command> allCommands = { command::set_size, command::pen_up, command::pen_down, command::move_left, command::move_right, command::move_up, command::move_down, command::move_to, command::set_color, command::fill, };
    const std::vector<command> commandsNoArgs = { command::pen_up, command::pen_down, command::fill, };
    const std::vector<command> commandsQtyArg = { command::move_left, command::move_right, command::move_up, command::move_down, };
    const std::vector<command> commandsXYArgs = { command::set_size, command::move_to, };
    const std::vector<command> commandsColorArg = { command::set_color, };
    const std::vector<color> allColors = {
        color::black,
        color::white,
        color::dark_gray,
        color::gray,
        color::light_gray,
        color::red,
        color::green,
        color::blue,
        color::cyan,
        color::magenta,
        color::yellow,
        color::dark_red,
        color::dark_green,
        color::dark_blue,
        color::dark_cyan,
        color::dark_magenta,
        color::dark_yellow,
    };

    const std::map<QString, command> m_textToCommand = {
        { QStringLiteral("set size"     ), command::set_size    },
        { QStringLiteral("pen up"       ), command::pen_up      },
        { QStringLiteral("pen down"     ), command::pen_down    },
        { QStringLiteral("move left"    ), command::move_left   },
        { QStringLiteral("move right"   ), command::move_right  },
        { QStringLiteral("move up"      ), command::move_up     },
        { QStringLiteral("move down"    ), command::move_down   },
        { QStringLiteral("move to"      ), command::move_to     },
        { QStringLiteral("set color"    ), command::set_color   },
        { QStringLiteral("fill"         ), command::fill        },
    };

    const std::map<QString, color> m_textToColor = {
        { QStringLiteral("black"        ), color::black         },
        { QStringLiteral("white"        ), color::white         },
        { QStringLiteral("dark gray"    ), color::dark_gray     },
        { QStringLiteral("gray"         ), color::gray          },
        { QStringLiteral("light gray"   ), color::light_gray    },
        { QStringLiteral("red"          ), color::red           },
        { QStringLiteral("green"        ), color::green         },
        { QStringLiteral("blue"         ), color::blue          },
        { QStringLiteral("cyan"         ), color::cyan          },
        { QStringLiteral("magenta"      ), color::magenta       },
        { QStringLiteral("yellow"       ), color::yellow        },
        { QStringLiteral("dark red"     ), color::dark_red      },
        { QStringLiteral("dark green"   ), color::dark_green    },
        { QStringLiteral("dark blue"    ), color::dark_blue     },
        { QStringLiteral("dark cyan"    ), color::dark_cyan     },
        { QStringLiteral("dark magenta" ), color::dark_magenta  },
        { QStringLiteral("dark yellow"  ), color::dark_yellow   },
    };

    const std::map<QString, pen> m_textToPen = {
        { QStringLiteral("up")  , pen::up   },
        { QStringLiteral("down"), pen::down },
    };
} //namespace crem

#endif // LANGUAGE_H
