#include "utils.h"

bool crem::utils::contained(command cmd, const std::vector<command> &commands)
{
    return std::find(std::cbegin(commands),
                     std::cend(commands),
                     cmd) != std::cend(commands);
}
